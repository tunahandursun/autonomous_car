import threading
import cv2
import sys
import RPi.GPIO as GPIO
from time import sleep
from time import time
import numpy as np
import serial

image_ready = None
GPIO.setmode(GPIO.BOARD)
cap = cv2.VideoCapture(0)
myserial = serial.Serial('/dev/ttyUSB0')
center_of_image_x = cap.read()[1].shape[1] / 2
distance_of_forward=0
GPIO_TRIGGER_1 = 37
GPIO_ECHO_1    = 35
GPIO.setup(GPIO_TRIGGER_1,GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO_1,GPIO.IN)      # Echo
def ultrasonic_forward():
    global GPIO_ECHO_1
    global GPIO_TRIGGER_1
    global distance_of_forward
    try:
        while (True):
            temperature = 20
            speedSound = 33100 + (0.6*temperature)
            GPIO.output(GPIO_TRIGGER_1, False)
            sleep(0.5)
            GPIO.output(GPIO_TRIGGER_1, True)
            sleep(0.00001)
            GPIO.output(GPIO_TRIGGER_1, False)
            start_1 = time()
            while GPIO.input(GPIO_ECHO_1)==0:
                start_1 = time()
            while GPIO.input(GPIO_ECHO_1)==1:
                stop_1 = time()
            elapsed_1 = stop_1-start_1
            distance_1 = (elapsed_1 * speedSound)/2
            distance_of_forward = distance_1
    except KeyboardInterrupt:
        sys.exit()
def camera_reader():
    global image_ready
    global img
    global cap
    #image_ready = 0
    while True:
        img = cap.read()[1]
    #image_ready = 1
class controlls:
    left = b'4'
    right = b'6'
    forward = b'8'
    stop = b'5'
t = threading.Thread(target = ultrasonic_forward)
t.start()
t2 = threading.Thread(target=camera_reader)
t2.start()
sayac=0
while sayac<5:
    sleep(1)
    print ("Programın açılmasına son" , 5-sayac , "saniye")
    sayac+=1
try:
    while True:

        lower = (0,120,0)
        upper = (255,255,255)
        hsv    = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        kernel = np.ones((5,5), np.uint8)
        mask   = cv2.inRange(hsv, lower, upper)
        mask   = cv2.erode(mask, kernel, iterations=1)
        mask   = cv2.dilate(mask, kernel, iterations=1)
        mask   = cv2.dilate(mask, kernel, iterations=1)
        mask   = cv2.erode(mask, kernel, iterations=1)
        konum  = cv2.moments(mask)
        alan   = int (konum['m00']);
        if alan >= 1000 and distance_of_forward>=15:
            center_of_object_x = int(konum['m10']/konum['m00'])
            print("alan 1000den büyük")
            if(center_of_object_x>center_of_image_x+190):
                print ("right çalışıyor")
                myserial.write(controlls.right)
                sleep(0.25)
                myserial.write(controlls.stop)
                sleep(0.15)
            elif(center_of_object_x<center_of_image_x-190):
                print ("left çalışıyor")
                myserial.write(controlls.left)
                sleep(0.25)
                myserial.write(controlls.stop)
                sleep(0.15)

            else:
                print ("forward çalışıyor")
                myserial.write(controlls.forward)
                sleep(0.25)
                myserial.write(controlls.stop)
                sleep(0.15)

            print (alan)
        else:
            print ("stop çalışlıyor")
            myserial.write(controlls.stop)
except KeyboardInterrupt:
    myserial.write(controlls.stop)
    sys.exit()
