#-*- coding: utf-8 -*-
from temel_hareketler import *
from ctypes import *
from time import asctime
import logging

class slowly(temel_hareketler):
	add = None
	plate_is_found = 0
	temp_for_gyro_value_control = None
	sensibility_of_gyro_value = 7
	rotation_speed = 18
	speed_of_forward = 20
	def __init__(self):
		self.importing_gyro()
		self.temp_for_gyro_value_control = self.add.main(0)
	def importing_gyro(self):
		logging.basicConfig(filename="mylog.log",level=logging.DEBUG)
		self.add = CDLL('./gyro.so')
		self.add.main()
		for i in range(0,20):
			print (i)
			time.sleep(1)
		logging.info("{} gyro imported".format(asctime()))
	def right(self,rotation_angle):
		logging.basicConfig(filename="mylog.log",level=logging.DEBUG)
		my_angle=self.add.main(0)
		temp_my_angle=my_angle
		#print("right çalıştı")
		logging.info("{} right çalıştı".format(asctime()))
		if self.plate_is_found:
			while not(temp_my_angle < my_angle + rotation_angle +2 and temp_my_angle > my_angle + rotation_angle - 2) and self.plate_is_found:
				#print("temp_my_angle: ", temp_my_angle,"goal:", my_angle+rotation_angle)
				logging.info("temp_my_angle: ", temp_my_angle,"goal:", my_angle+rotation_angle)
				if self.gyro_value_control(temp_my_angle):
					self.sol(self.rotation_speed)
				else:
					#print ("esgeç,right")
					logging.warning("{} esgeç".format(asctime()))
				time.sleep(0.01)
				temp_my_angle=self.add.main(0)
	def forward(self):
		logging.basicConfig(filename="mylog.log",level=logging.DEBUG)
		logging.info("{} forward çalışıyor".format(asctime()))
		self.sol(self.speed_of_forward)
		self.sag(self.speed_of_forward + 2.5)

		# while sayac < 25:
		# 	if(biseyler belirlendi):
		# 		break
		# 	else:
		# 		time.sleep(0.1)
		# 		sayac++

		time.sleep(0.25)
		logging.info("{} forward durduruluyor".format(asctime()))
		self.sol(0)
		self.sag(0)
		logging.info("{} forward durdu".format(asctime()))
	def stop(self):
		logging.basicConfig(filename="mylog.log",level=logging.DEBUG)
		#print("stopped")
		logging.warning("{} stopped".format(asctime()))
		self.sol(0)
		self.sag(0)
	def left(self,rotation_angle):
		logging.basicConfig(filename="mylog.log",level=logging.DEBUG)
		logging.info("{} left çalıştı".format(asctime))
		my_angle=self.add.main(0)
		temp_my_angle=my_angle
		#print("left çalıştı")
		if self.plate_is_found:
			while not(temp_my_angle < my_angle - rotation_angle +2 and temp_my_angle > my_angle - rotation_angle - 2) and self.plate_is_found:
				#print("temp_my_angle: ", temp_my_angle,"goal:", my_angle-rotation_angle)
				logging.info("{} temp_my_angle: {}, goal: {}".format(asctime(),temp_my_angle,my_angle-rotation_angle))
				if self.gyro_value_control(temp_my_angle):
					self.sag(self.rotation_speed)
				else:
					logging.warning("{} esgeç".format(asctime()))
					#print ("esgeç,left")
				time.sleep(0.01)
				temp_my_angle=self.add.main(0)
		else:
			return
	def myang(self):
		while(1):
			print (self.add.main(0))
	def gyro_value_control(self,temp):
		if abs(self.temp_for_gyro_value_control) - abs(temp) > self.sensibility_of_gyro_value:
			return 0
		else:
			self.temp_for_gyro_value_control=temp
			return 1
# with slowly() as a:
# 	a.plate_is_found=1
# 	a.right(100)
