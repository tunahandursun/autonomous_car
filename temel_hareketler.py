#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import sys
import time
import socket

GPIO.setmode(GPIO.BOARD)

class temel_hareketler:
	clientsocket = 	None

	sol_pin_1 = 	None
	sol_pin_2 = 	None
	sol_pin_pwm = 	None
	sag_pin_1 = 	None
	sag_pin_2 = 	None
	sag_pin_pwm = 	None

	pwm_sag = 		None
	pwm_sol = 		None

	hiz = 10
	artis_miktari = 10

	def __enter__(self):
		self.sol_pin_1 = 36
		self.sol_pin_2 = 38
		self.sol_pin_pwm   = 40
		self.sag_pin_1 =  11
		self.sag_pin_2 = 15
		self.sag_pin_pwm   = 7
		self.init_sag()
		self.init_sol()
		#self.pwm_sol.ChangeFrequency(10)
		#self.pwm_sag.ChangeFrequency(10)
		return self

	def __exit__(self, exc_type, exc_value, traceback):
		self.pwm_sag.ChangeDutyCycle(0)
		self.pwm_sol.ChangeDutyCycle(0)
		GPIO.cleanup()

	def hizlan(self):
		if self.hiz > -10:
			self.ileri()
			if self.hiz < 0:  # -10,+10 aralığında motorlar bir reaksiyon göstermediği için bu aralığı atlıyoz
				self.hiz = 10 # -10,+10 aralığında motorlar bir reaksiyon göstermediği için bu aralığı atlıyoz
		self.hiz = self.hiz + self.artis_miktari

		self.hiz_ata(abs(self.hiz))

	def yavasla(self):
		if self.hiz < 10:
			self.geri()
			if self.hiz > 0:    # -10,+10 aralığında motorlar bir reaksiyon göstermediği için bu aralığı atlıyoz
				self.hiz = -10 # -10,+10 aralığında motorlar bir reaksiyon göstermediği için bu aralığı atlıyoz
		self.hiz = self.hiz - self.artis_miktari
		self.hiz_ata(abs(self.hiz))

	def duzelt(self):
		self.hiz_ata(self.hiz)
		self.hiz_ata(0)

	def hiz_ata(self,hiz): # ikisine de aynı değeri ata
		self.pwm_sag.ChangeDutyCycle(hiz)
		self.pwm_sol.ChangeDutyCycle(hiz)

	def sol(self,hiz):
		try:
			self.pwm_sol.ChangeDutyCycle(hiz)
		except ValueError:
			print ("top speed")
		except:
			print ("sıkıntı var, temel_hareketler, sol")
	def sag(self,hiz):
		try:
			self.pwm_sag.ChangeDutyCycle(hiz)
		except ValueError:
			print ("top speed")
		except:
			print("sıkıntı var, temel_hareketler, sag")

	def init_sol(self): # pwm_sag dönderecek
		#---------- sol teker -------------------
		GPIO.setup(self.sol_pin_1,GPIO.OUT)
		GPIO.setup(self.sol_pin_2,GPIO.OUT)
		GPIO.setup(self.sol_pin_pwm,GPIO.OUT) #pwm

		GPIO.output(self.sol_pin_1,False)
		GPIO.output(self.sol_pin_2,True)

		self.pwm_sol = GPIO.PWM(self.sol_pin_pwm,50)

		self.pwm_sol.start(0)

	def init_sag(self): #pwm_sol dönderecek
		#---------- sag teker -------------------#
		GPIO.setup(self.sag_pin_1,GPIO.OUT)
		GPIO.setup(self.sag_pin_2,GPIO.OUT)
		GPIO.setup(self.sag_pin_pwm,GPIO.OUT) #pwm

		GPIO.output(self.sag_pin_1,False)
		GPIO.output(self.sag_pin_2,True)

		self.pwm_sag = GPIO.PWM(self.sag_pin_pwm,50)

		self.pwm_sag.start(0)

	def ileri(self):
		GPIO.output(self.sol_pin_1,False)
		GPIO.output(self.sol_pin_2,True)

		GPIO.output(self.sag_pin_1,False)
		GPIO.output(self.sag_pin_2,True)

	def geri(self):
		GPIO.output(self.sol_pin_1,True)
		GPIO.output(self.sol_pin_2,False)

		GPIO.output(self.sag_pin_1,True)
		GPIO.output(self.sag_pin_2,False)
