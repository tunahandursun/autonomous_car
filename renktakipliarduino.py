#-*- coding:utf-8 -*-
import cv2
import sys
from time import sleep
from time import asctime
import numpy as np
import threading
from random import randint
from slowly import *
import logging
import serial
plate_coordinate = None
plate_coordinate_middle = None
center_of_image_x = None
plate_is_found = 0
cap = cv2.VideoCapture(1)

GPIO_TRIGGER_1 = 37
GPIO_ECHO_1    = 35

class controlls:
    left = b'4'
    right = b'6'
    forward = b'8'
    stop = b'5'

def nothing(x):
    pass
def find_color():
    global cap
    lower = (95,201,189)
    upper = (115,255,255)

    while True:
        print("okunuyor")
        img = cap.read()[1]
        hsv    = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        kernel = np.ones((5,5), np.uint8)
        mask   = cv2.inRange(hsv, lower, upper)
        mask   = cv2.erode(mask, kernel, iterations=1)
        mask   = cv2.dilate(mask, kernel, iterations=1)
        mask   = cv2.dilate(mask, kernel, iterations=1)
        mask   = cv2.erode(mask, kernel, iterations=1)

        konum  = cv2.moments(mask)
        alan   = int (konum['m00']);
        if alan >= 100000:
            print("alan 10000den büyük")
            center_x = int(konum['m10']/konum['m00'])
            center_y = int(konum['m01']/konum['m00'])
            plate_is_found = 1
            plate_coordinate_middle = center_x

        else:
            plate_is_found = 0
        sleep(0.2)
def ultrasonic_forward():
    global GPIO_ECHO_1
    global GPIO_TRIGGER_1

    temperature = 20
    speedSound = 33100 + (0.6*temperature)
    GPIO.output(GPIO_TRIGGER_1, False)
    time.sleep(0.5)

    # Send 10us pulse to trigger
    GPIO.output(GPIO_TRIGGER_1, True)
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER_1, False)
    start_1 = time.time()
    while GPIO.input(GPIO_ECHO_1)==0:
        start_1 = time.time()
    while GPIO.input(GPIO_ECHO_1)==1:
        stop_1 = time.time()
    time.sleep(0.2)

    elapsed_1 = stop_1-start_1
    distance_1 = (elapsed_1 * speedSound)/2

    return distance_1
try:
    #center_of_image_x = cap.read()[1].shape[1] / 2

    t = threading.Thread(target = find_color)
    t.start()
    sleep(1)
    #
    #GPIO.setup(GPIO_TRIGGER_1,GPIO.OUT)  # Trigger
    #GPIO.setup(GPIO_ECHO_1,GPIO.IN)      # Echo
    myserial = serial.Serial('/dev/ttyUSB0')
    sleep(18)
    myserial.write(controlls.right)
    sleep(1)
    while True:
        try:
            #print("distance = ",temp_distance)
            if(plate_is_found):
                print("ileri")
                myserial.write(b'8')
                sleep(1)
            else:
                print("dur")
                myserial.write(b'5')
                sleep(1)

        except IndexError:
            pass

except KeyboardInterrupt:
    print ("except")
    pass
# finally:
#     GPIO.cleanup()
