#!/usr/bin/env python3
#-*- coding:utf-8 -*-


def alan_normalize(plakanin_alani):
    normalize = (plakanin_alani-1200.0)/(15500.0-1200.0)#8000 plakanın en yakın zamandaki alanı
    hiz = (1 - normalize)*30
    if hiz>=100:
        hiz=99
    elif hiz<0:
        hiz= 0
    print("deger: ",plakanin_alani, "normalize: ",hiz)
    return hiz
